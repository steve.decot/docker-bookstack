# docker-bookstack
## Creation : 20-02-2022


## Description
BookStack est un logiciel wiki gratuit et open source destiné à une plate-forme simple, auto-hébergée et facile à utiliser. Basé sur Laravel, un framework PHP, BookStack est publié sous la licence MIT. Il utilise les idées de livres pour organiser les pages et stocker des informations.

Bien que la vision de livre est présente dans la philosophie de l'app, vous pouvez y stocker tous types de documents. L'option de recherche est très performante (recherche classique, tag, dans du code d'une documentation ..)
## Contenu du Repos
Vous trouverez dans ce repos deux mode d'orchestration via un ```docker-compose.yml```:
* Le mode d'orchestration des service via un fichier .env qu'il faudra compléter (cf la section "La version avec le .env")
* Le mode d'orchestration classique via le fichier ```docker-compose_TO_EDIT_MAN.yml``` (cf la section "La version à editer manuellement" )

## Contenu de la Stack
La stack se base sur l'image de chez linuxserver(rootless,security...)
Cette dernière est couplée à un mariadb en version 10.6.2
Elle contient les label pour fonctionner avec un Traefik (présent dans mon gitlab ici --> https://gitlab.com/steve.decot/docker-traefik-protect-socket )
Il n'y a pas de service de gestion de mail ou de port smtp open, l'envoi de mail n'est actuellement pas fonctionnel. (prévu dans une prochaine maj )
### La version avec le .env
Les fichiers suivants sont couplés pour fonctionner ensembles
* ```docker-compose_with_env```
* ```.env```

La liste des variables suivantes doivent être affectées si vous passez par le .env
Voici la liste remplie à titre d'exemple
* ```BOOKSTACK_IMAGE=linuxserver/bookstack:21.12.5```
* ```CONTAINER_NAME_BOOKSTACK=bookstack_container```
* ```DB_USER=Billy```
* ```DB_PASS=Kimba```
* ```DB_DATABASE=birmingham```
* ```DOMAINTLD=billy-kimba.com```
* ```TRAEFIK_ROUTERS_NAME=routeur_bookstack```
* ```CONTAINER_NAME_MARIADB=mariadb-container```
* ```MARIADB_ROOT_PASSWORD=super_passwd_dur_a_trouver```
* ```IMAGE_MARIADB=mariadb:10.6.2```
### La version à editer manuellement
Le fichier ciblé est ```docker-compose_TO_EDIT_MAN.yml```
Changez les variables d'env, nom de container / service / domain_tld /router traefik à votre convenance ! 
(Pour ne pas vous perdre, utilisez la version avec le .env pour voir la liste à changer manuellement pour répondre à vos besoins/personnalisation)

### Au premier lancement
Le login par défaut est **admin@admin.com**  et le passwd est **password**
Connectez-vous, créez vous un user et supprimez/désactivez cet utilisateur 

### Prochainement
* Implémenter loki
* Implémenter mail 


### liens
* https://les-enovateurs.com/bookstack-plateforme-opensource/
* https://hub.docker.com/r/linuxserver/bookstack
* https://www.bookstackapp.com/
* https://hub.docker.com/r/linuxserver/bookstack
